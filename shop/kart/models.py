from django.contrib.auth.models import User
from django.db import models

# Create your models here.
class Products(models.Model):
    title=models.CharField(max_length=30)
    description = models.CharField(max_length=200)
    image = models.FileField(upload_to='photos/', blank=True, null=True)
    price = models.FloatField()
    createdon = models.DateField(auto_now_add=True)
    updatedon = models.DateField(auto_now=True)


    def __str__(self):
        return f"{self.id} -{self.title} - {self.description} - {self.image} - {self.price} - {self.createdon} - {self.updatedon}"

class Orders(models.Model):
    payment_choices = (
        ('cash', 'Cash'),
        ('paytm', 'Paytm'),
        ('card', 'Card'),
    )

    status_choices=(
        ('new','New'),
        ('paid','Paid')

    )

    userid = models.ForeignKey(User, on_delete=models.CASCADE)
    total = models.IntegerField()
    createdon = models.DateField(auto_now_add=True)
    updatedon = models.DateField(auto_now=True)
    status = models.CharField(max_length=20,choices=status_choices)
    mode_of_payments = models.CharField(max_length=30, choices=payment_choices)

    def _str_(self):
        return f"{self.id} -{self.userid} - {self.total} - {self.createdon} - {self.updatedon} - {self.status} -{self.mode_of_payments}"


class OrderItems(models.Model):
    orderid = models.ForeignKey(Orders, on_delete=models.CASCADE)
    productid = models.ForeignKey(Products, on_delete=models.CASCADE)
    quantity = models.IntegerField()
    price = models.FloatField()

    def _str_(self):
        return f"{self.id}-{self.orderid} -{self.productid} - {self.quantity} - {self.price}"