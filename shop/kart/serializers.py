from rest_framework import serializers

from kart.models import Products
from kart.models import Orders
from kart.models import OrderItems


class KartSerializer(serializers.ModelSerializer):
    class Meta:
        model = Products
        fields = ('id','title', 'description','image','price','createdon','updatedon')


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Orders
        fields = ('id','userid', 'total','createdon','updatedon','status','mode_of_payments')


class OrderitemsSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderItems
        fields = ('id','orderid','productid', 'quantity','price')


from rest_framework import serializers
from django.contrib.auth.models import User

# User Serializer
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email')

# Register Serializer
class RegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'password')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User.objects.create_user(validated_data['username'], validated_data['email'], validated_data['password'])

        return user