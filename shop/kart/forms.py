import django.forms as forms
from .models import Products
from .models import Orders
from .models import OrderItems
class ProductsForm(forms.ModelForm):
    class Meta:
        model = Products
        fields = '__all__'


class orderForm(forms.ModelForm):
    class Meta:
        model = Orders
        fields = '__all__'

class orderitemForm(forms.ModelForm):
    class Meta:
        model = OrderItems
        fields = '__all__'