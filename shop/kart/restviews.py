from django.contrib.auth.models import User
from rest_framework.authentication import BasicAuthentication, TokenAuthentication

from kart.models import Products,Orders,OrderItems
from kart.serializers import KartSerializer,OrderSerializer,OrderitemsSerializer

from rest_framework import generics
from rest_framework.permissions import IsAdminUser, IsAuthenticated, IsAuthenticatedOrReadOnly


class ProductsList(generics.ListCreateAPIView):
    def get_queryset(self):
        return Products.objects.filter(user=self.request.user)

        # Called before a new entry is inserted

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    serializer_class = KartSerializer
    # authentication_classes = [TokenAuthentication]
    # permission_classes = [IsAuthenticated]

class ProductsDetails(generics.RetrieveUpdateDestroyAPIView):
    def get_queryset(self):
        return Products.objects.filter(user=self.request.user)

    serializer_class = KartSerializer
    # authentication_classes = [BasicAuthentication]
    # authentication_classes = [TokenAuthentication]
    # permission_classes = [IsAuthenticatedOrReadOnly]


class OrderList(generics.ListCreateAPIView):
    def get_queryset(self):
        return Orders.objects.filter(user=self.request.user)

        # Called before a new entry is inserted

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    serializer_class = OrderSerializer
    # authentication_classes = [BasicAuthentication]
    # authentication_classes = [TokenAuthentication]
    # permission_classes = [IsAuthenticated]

class OrderDetails(generics.RetrieveUpdateDestroyAPIView):
    def get_queryset(self):
        return Orders.objects.filter(user=self.request.user)

    serializer_class = OrderSerializer
    # authentication_classes = [BasicAuthentication]
    # authentication_classes = [TokenAuthentication]
    # permission_classes = [IsAuthenticatedOrReadOnly]


class OrderitemsList(generics.ListCreateAPIView):
    def get_queryset(self):
        return OrderItems.objects.filter(user=self.request.user)

        # Called before a new entry is inserted

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    serializer_class = OrderitemsSerializer
    # authentication_classes = [BasicAuthentication]
    # authentication_classes = [TokenAuthentication]
    # permission_classes = [IsAuthenticated]

class OrderitemsDetails(generics.RetrieveUpdateDestroyAPIView):
    def get_queryset(self):
        return OrderItems.objects.filter(user=self.request.user)

    serializer_class = OrderitemsSerializer
    # authentication_classes = [BasicAuthentication]
    # authentication_classes = [TokenAuthentication]
    # permission_classes = [IsAuthenticatedOrReadOnly]



from rest_framework import generics, permissions
from rest_framework.response import Response
from knox.models import AuthToken
from .serializers import UserSerializer, RegisterSerializer

# Register API
class RegisterAPI(generics.GenericAPIView):
    serializer_class = RegisterSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        return Response({
        "user": UserSerializer(user, context=self.get_serializer_context()).data,
        "token": AuthToken.objects.create(user)[1]
        })


from django.contrib.auth import login

from rest_framework import permissions
from rest_framework.authtoken.serializers import AuthTokenSerializer
from knox.views import LoginView as KnoxLoginView

class LoginAPI(KnoxLoginView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, format=None):
        serializer = AuthTokenSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        login(request, user)
        return super(LoginAPI, self).post(request, format=None)