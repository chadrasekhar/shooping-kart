from django.contrib import admin
from kart.models import *

# Register your models here.

admin.site.register(Products)
admin.site.register(Orders)
admin.site.register(OrderItems)


