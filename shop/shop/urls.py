"""shop URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from kart import restviews
from kart.restviews import RegisterAPI
from knox import views as knox_views
from kart.restviews import LoginAPI

urlpatterns = [
    path('admin/', admin.site.urls),
    path("rest/products/", restviews.ProductsList.as_view()),
    path("rest/products/<int:pk>", restviews.ProductsDetails.as_view()),

    path("rest/order/", restviews.OrderList.as_view()),
    path("rest/order/<int:pk>", restviews.OrderDetails.as_view()),


    path("rest/orderitems/", restviews.OrderitemsList.as_view()),
    path("rest/orderitems/<int:pk>", restviews.OrderitemsDetails.as_view()),


    path('api/register/', RegisterAPI.as_view(), name='register'),
    path('api/login/', LoginAPI.as_view(), name='login'),
    path('api/logout/', knox_views.LogoutView.as_view(), name='logout'),
    path('api/logoutall/', knox_views.LogoutAllView.as_view(), name='logoutall'),
]
